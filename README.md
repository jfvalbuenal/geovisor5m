**Para visualizar el mapa dinámico haga clic [aquí](https://jfvalbuenal.gitlab.io/geovisor5m/)**

# Geovisor de avance: Clasificación de unidades hidrogeológicas SGC, basado en metodología CPRM-Brasil

El visualizador reúne los avances en la 
tarea por clasificar de menara un mapa hidrogeológico para Colombia escala 1:5M

El visor es construido sobre `R 4.2.1`

A su vez se han requerido funcionalidades específicas de las librerías:

 - leafem_0.2.0
 - spatialEco_1.3-7  
 - htmlwidgets_1.5.4
 - htmltools_0.5.2
 - maptools_1.1-4
 - rgeos_0.5-9
 - here_1.0.1
 - raster_3.5-15
 - rgdal_1.5-32
 - sp_1.4-5
 - dplyr_1.0.4
 - leaflet_2.1.1
 - sf_1.0-8     


## Contenido
El geovisor está compuesto de 3 módulos, un script principal y dos auxiliares, la ejecución de realiza desde el módulo  `VisorMapWeb.R` y su resultado se guarda como archivo `.html` en la carpeta raíz de trabajo. La información temática agrupada se ha centrado en la presentación de *Grupos hidrogeológicos*, *Sistemas acuíferos* y *niveles de confianza*.

Cada una de estas visualizaciones contiene comentarios explicativos por polígonos, desplegables al realizar clic sobre las unidades.     

